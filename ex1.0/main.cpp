#include <iostream>
#include "threads.h"


int main()
{

	callWritePrimesMultipleThreads(0, 1000, "tester.txt", 100);
	callWritePrimesMultipleThreads(0, 100000, "tester.txt", 100);
	callWritePrimesMultipleThreads(0, 1000000, "tester.txt", 100);

	return 0;
}
