#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <functional>
#include <mutex>


// write prime nymber of range begin - end to the file
void writePrimesToFile(int begin, int end, std::ofstream& file);
// displaye the range to n range and make n thread of  writePrimesToFile func
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
