
#include "threads.h"

std::mutex m;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int flag;
	while (begin < end)
	{
		flag = 0;
		for (int i = 2, j = sqrt(begin); i <= j; i++)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (begin && flag == 0 && begin != 1)
		{
			std::unique_lock<std::mutex> locker(m);
			file << begin << "\n";
			locker.unlock();
		}
		begin++;
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	std::function<void(int begin, int end, std::ofstream & file)> f;

	if (N < 1 || begin < 0 || end < 2)
	{
		return;
	}
	auto start = std::chrono::system_clock::now();// start the stoper
	for (int i = 0, begin2 = 0, end2 = (end / N); i < N; i++, begin2 += (end / N), end2 += (end / N))
	{
		f = writePrimesToFile;
		f(begin2, end2, file);
		std::thread(f);
	}
	auto endd = std::chrono::system_clock::now();// stoping the stoper
	std::chrono::duration<double> elapsed_seconds = endd - start;
	std::cout << "time: " << elapsed_seconds.count() << std::endl;
}
