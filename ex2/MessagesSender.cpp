#include "MessagesSender.h"


MessagesSender::MessagesSender()
{
	_finished = 1;
}

MessagesSender::~MessagesSender()
{
	_finished = 0;// to release the two threads
	std::cout << "Waiting to the thread...\n";
	std::this_thread::sleep_for(std::chrono::seconds(60));
}

void MessagesSender::printMenue()
{
	std::cout << "1. Signin\n2. Signout\n3. Connected Users\n4. exit\n";
}

void MessagesSender::signin()
{
	std::string name;
	std::cout << "Enter your name:\n";
	std::cin >> name;
	
	if (_connectedUser.find(name) != _connectedUser.end())
	{
		std::cout << "Your are alredy connected\n";
		return;
	}
	_connectedUser.insert(name);
}

void MessagesSender::signout()
{
	std::string name;
	std::cout << "Enter your name:\n";
	std::cin >> name;

	if (_connectedUser.find(name) != _connectedUser.end())
	{
		_connectedUser.erase(name);
	}
}

void MessagesSender::printConectUser()
{
	std::cout << "connected users:\n";
	for (auto it = _connectedUser.begin(); it != _connectedUser.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
}

bool MessagesSender::is_empty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

void MessagesSender::readDataFile()
{
	std::ifstream file;
	std::string line;
	file.open("data.txt");
	int i = 0;

	while (_finished)
	{
		if (!is_empty(file))
		{
			while (getline(file, line))
			{
				std::unique_lock<std::mutex> locker(_m);
				_data.push(line);
				locker.unlock();
			}
			_cond.notify_one();
		}
		file.close();
		file.open("data.txt", std::ofstream::out | std::ofstream::trunc);
		file.close();
		std::this_thread::sleep_for(std::chrono::seconds(60));
		file.open("data.txt");
	}

	file.close();
}

void MessagesSender::sendMessage()
{
	std::string line;
	std::ofstream myFile("output.txt");

	while (_finished)
	{
		std::unique_lock<std::mutex> locker(_m);
		_cond.wait(locker);

		for (int i = 0; i < _data.size(); i++)
		{
			line = _data.front();
			_data.pop();
			for (auto it = _connectedUser.begin(); it != _connectedUser.end(); ++it)
			{
				myFile << *it << ": " << line << std::endl;
			}
		}
		locker.unlock();
	}

	myFile.close();
}

std::thread MessagesSender::readThread()
{
	return std::thread([this] { this->readDataFile(); });
}

std::thread MessagesSender::sendThread()
{
	return std::thread( [this] { this->sendMessage(); });
}




