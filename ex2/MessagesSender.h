#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <set> 
#include <iterator> 
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <Windows.h>
#include <queue>
#include <mutex>

class MessagesSender
{
public:

	// c'tor, d'tor
	MessagesSender();
	~MessagesSender();
	//menue
	void printMenue();
	void signin();
	void signout();
	void printConectUser();
	//check if file is empty
	bool is_empty(std::ifstream& pFile);
	// read data frome data.txt file all 60 secondes
	void readDataFile();
	// write data frome qeueu in output.txt file
	void sendMessage();
	// return thread of the two function: read and send
	std::thread readThread();
	std::thread sendThread();

private:

	std::set<std::string> _connectedUser;
	std::queue<std::string> _data;
	std::mutex _m;
	std::condition_variable _cond;
	int _finished;
};









