#include "MessagesSender.h"

int main()
{
	MessagesSender a;
	int choose = 0;

	std::thread t1 = a.readThread();
	t1.detach();
	std::thread t2 = a.sendThread();
	t2.detach();

	while (choose != 4)
	{
		a.printMenue();
		std::cin >> choose;
		switch (choose)
		{
			case 1:
			{
				a.signin();
				break;
			}
			case 2:
			{
				a.signout();
				break;
			}
			case 3:
			{
				a.printConectUser();
				break;
			}
			default:
			{
				break;
			}
		}
	}

	return 0;
}













